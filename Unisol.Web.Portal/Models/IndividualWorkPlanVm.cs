﻿using System;

namespace Unisol.Web.Portal.Models
{
    public class IndividualWorkPlanVm
    {
        public string StaffNo { get; set; }
        public Guid Id { get; set; }
    }
}
