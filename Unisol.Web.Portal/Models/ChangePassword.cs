﻿
namespace Unisol.Web.Portal.Models
{
    public class ChangePassword
    {
        public string newPassword { get; set; }
        public string oldPassword { get; set; }
        public string userName { get; set; }
    }
}
