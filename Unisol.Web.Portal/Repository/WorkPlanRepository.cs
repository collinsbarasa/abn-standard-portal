﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.ViewModels.Profile;
using Unisol.Web.Entities.Database.MembershipModels;
using Unisol.Web.Entities.Database.UnisolModels;
using Unisol.Web.Entities.Model;
using Unisol.Web.Portal.IRepository;
using Unisol.Web.Portal.IServices;

namespace Unisol.Web.Portal.Repository
{
    public class WorkPlanRepository : IWorkPlanRepository
    {
        private readonly PortalCoreContext _context;
        private readonly IUnisolApiProxy _unisolApiProxy;
        public WorkPlanRepository(PortalCoreContext context, IUnisolApiProxy unisolApiProxy) {
            _context = context;
            _unisolApiProxy = unisolApiProxy;
        }
        public ReturnData<IndividualWorkPlan> CreateIndividualWorkPlan(string staffNo, Guid id)
        {
            
            try {
                var activeWorkPlan = _context.DepartmentWorkPlans
                    .Include(i=>i.WorkPlanObjectives)
                    .FirstOrDefault(x=>x.Published == true && x.Status == "Active"  && x.Id == id);
                if (activeWorkPlan == null) {
                    return new ReturnData<IndividualWorkPlan>
                    {
                        Success = false,
                        Message = "We are unable to create a workplan from the selected departmental workplan.It might not be set to active or is yet to be published.",
                    };
                }

                var individualWP = new IndividualWorkPlan(activeWorkPlan);
                individualWP.StaffNo = staffNo;
                individualWP.OfficerInCharge = staffNo;
                _context.IndividualWorkPlans.Add(individualWP);
                _context.SaveChanges();

                return new ReturnData<IndividualWorkPlan>
                {
                    Success = true,
                    Message = "Success",
                    Data = individualWP
                };
            } catch (Exception ex) {
                return new ReturnData<IndividualWorkPlan> { 
                    Success = false,
                    Message = new Error(ex).Message,
                };
            }
        }

        public ReturnData<DepartmentWorkPlan> CreateWorkPlan(DepartmentWorkPlan workPlan)
        {
            try {
                workPlan.Id = Guid.NewGuid();
                workPlan.Status = "Pending";
                workPlan.Published = false;
                workPlan.WorkPlanObjectives = new List<WorkPlanObjective>();
                _context.DepartmentWorkPlans.Add(workPlan);
                _context.SaveChanges();

                return new ReturnData<DepartmentWorkPlan> { 
                    Success = true,
                    Message = "Success",
                    Data = workPlan
                };
            } catch (Exception ex) {
                return new ReturnData<DepartmentWorkPlan> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanObjective> CreateWorkPlanObjective(WorkPlanObjective model) {
            try
            {
                var deptWorkPlan = _context.DepartmentWorkPlans
                    .Include(i=>i.WorkPlanObjectives)
                    .FirstOrDefault(x => x.Id == model.WorkPlanId);
                var objective = new WorkPlanObjective();
                objective.Description = model.Description;
                objective.WorkPlanActivities = new List<WorkPlanActivity>();
                if (deptWorkPlan != null) {
                    
                    deptWorkPlan.WorkPlanObjectives.Add(objective);
                }

                if (deptWorkPlan == null) {
                    var iWorkPlan = _context.IndividualWorkPlans
                    .Include(i => i.WorkPlanObjectives)
                    .FirstOrDefault(x => x.Id == model.WorkPlanId);
                    if (iWorkPlan != null) {
                        iWorkPlan.WorkPlanObjectives.Add(objective);
                    }
                }
                
                _context.SaveChanges();

                return new ReturnData<WorkPlanObjective>()
                {
                    Success = true,
                    Message = "Success",
                    Data = new WorkPlanObjective()
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanObjective>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivity> CreateWorkPlanActivity(WorkPlanActivityVm model)
        {
            try
            {
                var objective = _context.WorkPlanObjectives
                    .Include(i=>i.WorkPlanActivities)
                    .FirstOrDefault(x=>x.Id == model.ObjectiveId);
                var activity = new WorkPlanActivity(model);
                objective.WorkPlanActivities.Add(activity);
                _context.SaveChanges();

                return new ReturnData<WorkPlanActivity>() { 
                    Success = true,
                    Message = "Success",
                    Data = activity
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanActivity>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivityEvaluation> CreateWorkPlanActivityEvaluation(WorkPlanActivityEvaluationVm model)
        {
            try {
                var activity = _context.WorkPlanActivities
                    .Include(i=>i.MonitoringEvaluations)
                    .FirstOrDefault(x=>x.Id == model.ActivityId);
                var evaluation = new WorkPlanActivityEvaluation { 
                    Id = Guid.NewGuid(),
                    Description = model.Description
                };
                activity.MonitoringEvaluations.Add(evaluation);
                _context.SaveChanges();
                return new ReturnData<WorkPlanActivityEvaluation> { 
                    Success = true,
                    Message = "Success",
                    Data = evaluation
                };
            } catch (Exception ex) {
                return new ReturnData<WorkPlanActivityEvaluation> { 
                    Success = false,
                    Message = new Error(ex).Message,
                };
            }
        }

        public ReturnData<List<StaffProfileDataVm>> CreateWorkplanActivityOfficers(WorkPlanActivityOfficerVm model)
        {
            try {
                var officers = new List<StaffProfileDataVm>();
                var activity = _context.WorkPlanActivities
                    .Include(i=>i.Officers)
                    .FirstOrDefault(x => x.Id == model.ActivityId);
                foreach (var staffNo in model.StaffNumbers) {
                    var staffDetails = _unisolApiProxy.GetStaffData(staffNo).Result;
                    var jdata = JsonConvert.DeserializeObject<ReturnData<StaffProfileDataVm>>(staffDetails);
                    if (jdata.Success)
                    {
                        officers.Add(jdata.Data);

                        var officer = new WorkPlanActivityOfficer
                        {
                            Id = Guid.NewGuid(),
                            StaffNo = staffNo
                        };
                        activity.Officers.Add(officer);
                        _context.SaveChanges();
                    }
                }
                return new ReturnData<List<StaffProfileDataVm>> { 
                    Success = true,
                    Message = "Success",
                    Data = officers
                };
            } catch (Exception ex) {
                return new ReturnData<List<StaffProfileDataVm>> { 
                    Success= false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivityOutput> CreateWorkPlanActivityOutput(WorkPlanActivityOutputVm model)
        {
            try {
                var activity = _context.WorkPlanActivities
                    .Include(i=>i.OutPuts)
                    .FirstOrDefault(x => x.Id == model.ActivityId);
                var output = new WorkPlanActivityOutput { 
                    Id = Guid.NewGuid(),
                    Description = model.Description
                };
                activity.OutPuts.Add(output);
                _context.SaveChanges();
                return new ReturnData<WorkPlanActivityOutput>
                {
                    Success = true,
                    Message = "Success",
                    Data = output
                };

            } catch (Exception ex) {
                return new ReturnData<WorkPlanActivityOutput>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivity> DeleteWorkPlanActivity(Guid id)
        {
            try {
                var activity = _context.WorkPlanActivities.FirstOrDefault(x => x.Id == id);
                if (activity == null) { 
                     return new ReturnData<WorkPlanActivity>
                    {
                        Success = false,
                        Message = "Activity not found."
                    };
                }

                _context.WorkPlanActivities.Remove(activity);
                _context.SaveChanges();
                return new ReturnData<WorkPlanActivity>
                {
                    Success = true,
                    Message = "Success",
                    Data = activity
                };
            } catch (Exception ex) {
                return new ReturnData<WorkPlanActivity> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }
        public ReturnData<WorkPlanObjective> DeleteWorkPlanObjective(Guid id)
        {
            try
            {
                var objective = _context.WorkPlanObjectives.FirstOrDefault(x => x.Id == id);
                if (objective == null)
                {
                    return new ReturnData<WorkPlanObjective>
                    {
                        Success = false,
                        Message = "Activity not found."
                    };
                }

                _context.WorkPlanObjectives.Remove(objective);
                _context.SaveChanges();
                return new ReturnData<WorkPlanObjective>
                {
                    Success = true,
                    Message = "Success",
                    Data = objective
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanObjective>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<bool> Publish(Guid id, string action)
        {
            try
            {
                var workPlan = _context.DepartmentWorkPlans.FirstOrDefault(x => x.Id == id);
                if (workPlan == null)
                {
                    return new ReturnData<bool>
                    {
                        Success = false,
                        Message = "Activity not found."
                    };
                }
                if (action == "publish") {
                    workPlan.Published = true;
                }
                if (action == "unpublish")
                {
                    workPlan.Published = false;
                }
                _context.SaveChanges();
                return new ReturnData<bool>
                {
                    Success = true,
                    Message = "Success",
                    Data = true
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<bool>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<bool> Activate(Guid id, string action)
        {
            try
            {
                var workPlan = _context.DepartmentWorkPlans.FirstOrDefault(x => x.Id == id);
                if (workPlan == null)
                {
                    return new ReturnData<bool>
                    {
                        Success = false,
                        Message = "Workplan not found."
                    };
                }
                if (action == "activate")
                {
                    workPlan.Status = "Active";
                }
                if (action == "inactivate")
                {
                    workPlan.Status = "InActive";
                }
                _context.SaveChanges();
                return new ReturnData<bool>
                {
                    Success = true,
                    Message = "Success",
                    Data = true
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<bool>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }
        public ReturnData<WorkPlanActivityEvaluation> DeleteWorkPlanActivityEvaluation(Guid id)
        {
            try {
                var activity = _context.WorkPlanActivities
                    .Include(i => i.MonitoringEvaluations)
                    .FirstOrDefault(x=>x.MonitoringEvaluations.Any(y=>y.Id == id));
                if (activity == null) {
                    return new ReturnData<WorkPlanActivityEvaluation>
                    {
                        Success = false,
                        Message = "Activity with that evaluation not found."
                    };
                }

                var evaluation = activity.MonitoringEvaluations.FirstOrDefault(x=>x.Id == id);
                activity.MonitoringEvaluations.Remove(evaluation);
                _context.SaveChanges();

                return new ReturnData<WorkPlanActivityEvaluation> { 
                    Success = true,
                    Message = "Success",
                    Data = evaluation
                };
            } catch (Exception ex) {
                return new ReturnData<WorkPlanActivityEvaluation> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivityOutput> DeleteWorkPlanActivityOutput(Guid id)
        {
            try
            {
                var activity = _context.WorkPlanActivities
                    .Include(i => i.OutPuts)
                    .FirstOrDefault(x => x.OutPuts.Any(y => y.Id == id));
                if (activity == null)
                {
                    return new ReturnData<WorkPlanActivityOutput>
                    {
                        Success = false,
                        Message = "Activity with that output not found."
                    };
                }

                var output = activity.OutPuts.FirstOrDefault(x => x.Id == id);
                activity.OutPuts.Remove(output);
                _context.SaveChanges();

                return new ReturnData<WorkPlanActivityOutput>
                {
                    Success = true,
                    Message = "Success",
                    Data = output
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanActivityOutput>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<IndividualWorkPlan> EditIndividualWorkPlan(Guid id)
        {
            throw new NotImplementedException();
        }

        public ReturnData<DepartmentWorkPlan> EditWorkPlan(DepartmentWorkPlan model)
        {
            try {
                var workplan = _context.DepartmentWorkPlans.FirstOrDefault(x=>x.Id == model.Id);
                workplan.FinancialYear = model.FinancialYear;
                workplan.Department = model.Department;
                workplan.OfficerInCharge = model.OfficerInCharge;
                workplan.SectionUnit = model.SectionUnit;
                workplan.Status = model.Status;
                workplan.DateUpdated = DateTime.Now;
                _context.SaveChanges();
                return new ReturnData<DepartmentWorkPlan> { 
                    Success = true,
                    Message = "Success",
                    Data = workplan
                };
            } catch (Exception ex) {
                return new ReturnData<DepartmentWorkPlan> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivity> EditWorkPlanActivity(WorkPlanActivityVm model)
        {
            try {
                var activity = _context.WorkPlanActivities.FirstOrDefault(x=>x.Id == model.ObjectiveId);
                activity.Description = model.Description;
                activity.Budget = Convert.ToDouble(model.Budget);
                activity.ByDate = model.ByDate;
                activity.DateUpdated = DateTime.Now;
                activity.Quarter = model.Quarter;
                activity.Status = model.Status;
                _context.SaveChanges();

                return new ReturnData<WorkPlanActivity> { 
                    Success = true,
                    Message = "Success",
                    Data = activity
                };
            } catch (Exception ex) {
                return new ReturnData<WorkPlanActivity> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanObjective> EditWorkPlanObjective(WorkPlanObjective model)
        {
            try
            {
                var objective = _context.WorkPlanObjectives.FirstOrDefault(x => x.Id == model.WorkPlanId);
                objective.Description = model.Description;
                
                _context.SaveChanges();

                return new ReturnData<WorkPlanObjective>
                {
                    Success = true,
                    Message = "Success",
                    Data = objective
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanObjective>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivityEvaluation> EditWorkPlanActivityEvaluation(WorkPlanActivityEvaluationVm model)
        {
            try
            {
                var activity = _context.WorkPlanActivities
                    .Include(i => i.MonitoringEvaluations)
                    .FirstOrDefault(x => x.MonitoringEvaluations.Any(y => y.Id == model.ActivityId));
                if (activity == null)
                {
                    return new ReturnData<WorkPlanActivityEvaluation>
                    {
                        Success = false,
                        Message = "Activity with that evaluation not found."
                    };
                }
                activity.MonitoringEvaluations.FirstOrDefault(x => x.Id == model.ActivityId).Description = model.Description;
                activity.MonitoringEvaluations.FirstOrDefault(x => x.Id == model.ActivityId).DateUpdated = DateTime.Now;

                _context.SaveChanges();

                return new ReturnData<WorkPlanActivityEvaluation>
                {
                    Success = true,
                    Message = "Success",
                    Data = activity.MonitoringEvaluations.FirstOrDefault(x => x.Id == model.Id)
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanActivityEvaluation>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanActivityOutput> EditWorkPlanActivityOutput(WorkPlanActivityOutputVm model)
        {
            try
            {
                var activity = _context.WorkPlanActivities
                    .Include(i => i.OutPuts)
                    .FirstOrDefault(x => x.OutPuts.Any(i=>i.Id == model.ActivityId));
                if (activity == null)
                {
                    return new ReturnData<WorkPlanActivityOutput>
                    {
                        Success = false,
                        Message = "Activity with that output metric not found."
                    };
                }
                activity.OutPuts.FirstOrDefault(x => x.Id == model.ActivityId).Description = model.Description;
                activity.OutPuts.FirstOrDefault(x => x.Id == model.ActivityId).DateUpdated = DateTime.Now;

                _context.SaveChanges();

                return new ReturnData<WorkPlanActivityOutput>
                {
                    Success = true,
                    Message = "Success",
                    Data = activity.OutPuts.FirstOrDefault(x => x.Id == model.Id)
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanActivityOutput>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<List<IndividualWorkPlan>> GetIndividualWorkPlans(string financialYear = null, string department = null)
        {
            try {
                var plans = new List<IndividualWorkPlan>();
                if (financialYear == null && department == null) {
                    plans = _context.IndividualWorkPlans.ToList();
                }

                if (financialYear != null && department == null) {
                    plans = _context.IndividualWorkPlans.Where(x => x.FinancialYear == financialYear).ToList();
                }

                if (department != null && financialYear == null)
                {
                    plans = _context.IndividualWorkPlans.Where(x => x.Department == department).ToList();
                }
                if (financialYear != null && department != null)
                {
                    plans = _context.IndividualWorkPlans
                        .Where(x => x.FinancialYear == financialYear && x.Department == department).ToList();
                }

                return new ReturnData<List<IndividualWorkPlan>> { 
                    Success = true,
                    Message = "Success",
                    Data = plans
                };
            } catch (Exception ex) {
                return new ReturnData<List<IndividualWorkPlan>> { 
                    Success = false,
                    Message = new Error(ex).Message,
                };
            }
        }

        public ReturnData<IndividualWorkPlan> GetIndividualWorkPlan(Guid id)
        {
            try
            {
                var plan = _context.IndividualWorkPlans
                    .Include(x => x.WorkPlanObjectives)
                    .ThenInclude(i => i.WorkPlanActivities)
                    .FirstOrDefault(x => x.Id == id);

                return new ReturnData<IndividualWorkPlan> { 
                    Success = true,
                    Message = "Success",
                    Data = plan
                };
            } catch (Exception ex) {
                return new ReturnData<IndividualWorkPlan> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<DepartmentWorkPlan> GetWorkPlan(Guid id)
        {
            try
            {
                var plan = _context.DepartmentWorkPlans
                    .Include(x => x.WorkPlanObjectives)
                    .ThenInclude(i => i.WorkPlanActivities)
                    .FirstOrDefault(x => x.Id == id);

                return new ReturnData<DepartmentWorkPlan>
                {
                    Success = true,
                    Message = "Success",
                    Data = plan
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<DepartmentWorkPlan>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<List<DepartmentWorkPlan>> GetWorkPlans(string financialYear = null, string department = null)
        {
            try
            {
                var plans = new List<DepartmentWorkPlan>();
                if (financialYear == null && department == null)
                {
                    plans = _context.DepartmentWorkPlans.ToList();
                }

                if (financialYear != null && department == null)
                {
                    plans = _context.DepartmentWorkPlans.Where(x => x.FinancialYear == financialYear).ToList();
                }

                if (department != null && financialYear == null)
                {
                    plans = _context.DepartmentWorkPlans.Where(x => x.Department == department).ToList();
                }
                if (financialYear != null && department != null)
                {
                    plans = _context.DepartmentWorkPlans
                        .Where(x => x.FinancialYear == financialYear && x.Department == department).ToList();
                }

                return new ReturnData<List<DepartmentWorkPlan>>
                {
                    Success = true,
                    Message = "Success",
                    Data = plans
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<List<DepartmentWorkPlan>>
                {
                    Success = false,
                    Message = new Error(ex).Message,
                };
            }
        }

        public ReturnData<HrpEmployee> RemoveWorkPlanActivityOfficer(Guid id)
        {
            try {
                var activity = _context.WorkPlanActivities
                    .Include(o=>o.Officers)
                    .FirstOrDefault(x=>x.Officers.Any(i=>i.Id == id));
                var officer = activity.Officers.FirstOrDefault(x=>x.Id == id);
                activity.Officers.Remove(officer);
                _context.SaveChanges();
                return new ReturnData<HrpEmployee> { 
                    Success = true,
                    Message = "Success"
                };
            } catch (Exception ex) {
                return new ReturnData<HrpEmployee> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<WorkPlanObjective> GetWorkPlanObjective(Guid id)
        {
            try {
                return new ReturnData<WorkPlanObjective> {
                    Success = true,
                    Message = "Success",
                    Data = _context.WorkPlanObjectives
                    .Include(i => i.WorkPlanActivities)
                    .ThenInclude(a=>a.Officers)
                    .Include(i => i.WorkPlanActivities)
                    .ThenInclude(k=>k.MonitoringEvaluations)
                    .Include(i => i.WorkPlanActivities)
                    .ThenInclude(j=>j.OutPuts)
                    .FirstOrDefault(x=>x.Id == id)
                };
            } catch (Exception ex) {
                return new ReturnData<WorkPlanObjective> { 
                    Success = false,
                    Message = new Error(ex).Message,
                };
            }
        }

        public ReturnData<WorkPlanActivity> GetWorkPlanActivity(Guid id)
        {
            try
            {
                try
                {
                    return new ReturnData<WorkPlanActivity>
                    {
                        Success = true,
                        Message = "Success",
                        Data = _context.WorkPlanActivities
                        .Include(i => i.Officers)
                        .Include(i => i.OutPuts)
                        .Include(i => i.MonitoringEvaluations)
                        .FirstOrDefault(x => x.Id == id)
                    };
                }
                catch (Exception ex)
                {
                    return new ReturnData<WorkPlanActivity>
                    {
                        Success = false,
                        Message = new Error(ex).Message,
                    };
                }
            }
            catch (Exception ex)
            {
                return new ReturnData<WorkPlanActivity>
                {
                    Success = false,
                    Message = new Error(ex).Message,
                };
            }
        }

        public ReturnData<List<IndividualWorkPlan>> GetIndividualWorkPlans(string staffNo)
        {
            try {
                return new ReturnData<List<IndividualWorkPlan>>() {
                    Success = true,
                    Message = "Individual workplans",
                    Data = _context.IndividualWorkPlans.Where(x => x.StaffNo == staffNo).ToList()
                };
            } catch (Exception ex) {
                return new ReturnData<List<IndividualWorkPlan>> { 
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<IndividualWorkPlan> RequestApproval(Guid id, string action)
        {
            try
            {
                var plan = _context.IndividualWorkPlans
                    .FirstOrDefault(x => x.Id == id);
                if (action == "approve") {
                    plan.Status = "Pending Approval";
                }
                if (action == "cancel")
                {
                    plan.Status = "Created";
                }
                _context.SaveChanges();
                return new ReturnData<IndividualWorkPlan>
                {
                    Success = true,
                    Message = "Success",
                    Data = plan
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<IndividualWorkPlan>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

        public ReturnData<IndividualWorkPlan> Approve(Guid id, string action)
        {
            try
            {
                var plan = _context.IndividualWorkPlans
                    .FirstOrDefault(x => x.Id == id);
                if (action == "approve")
                {
                    plan.Status = "Approved";
                }
                if (action == "decline")
                {
                    plan.Status = "Declined";
                }
                _context.SaveChanges();
                return new ReturnData<IndividualWorkPlan>
                {
                    Success = true,
                    Message = "Success",
                    Data = plan
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<IndividualWorkPlan>
                {
                    Success = false,
                    Message = new Error(ex).Message
                };
            }
        }

    }
}
