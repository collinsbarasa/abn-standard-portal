﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unisol.Web.Common.Process;
using Unisol.Web.Entities.Database.MembershipModels;
using Unisol.Web.Portal.IServices;
using Unisol.Web.Portal.Utilities;

namespace Unisol.Web.Portal.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UtilitiesController : Controller
    {
        private readonly IUnisolApiProxy _unisolApiProxy;
        private readonly PortalCoreContext _context;
        private readonly TokenValidator _tokenValidator;
        public UtilitiesController(IUnisolApiProxy unisolApiProxy, PortalCoreContext context)
        {
            _unisolApiProxy = unisolApiProxy;
            _context = context;
            _tokenValidator = new TokenValidator(_context);
        }

        [HttpGet("GetDepartments")]
        public JsonResult GetDepartments()
        {
            var token = _tokenValidator.Validate(HttpContext);
            if (!token.Success)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    NotAuthenticated = true,
                    Message = $"Unauthorized:-{token.Message}",
                });
            }
            if (token.Role != Role.Admin)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    NotAuthenticated = true,
                    Message = "Sorry, you are not authorized to perform this action",
                });
            }
            var departments = _unisolApiProxy.GetDepartments().Result;
            var jdata = JsonConvert.DeserializeObject<ReturnData<List<string>>>(departments);
            return Json(jdata);
        }

        [HttpGet("GetFinancialYears")]
        public JsonResult GetFinancialYears()
        {
            var token = _tokenValidator.Validate(HttpContext);
            if (!token.Success)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    NotAuthenticated = true,
                    Message = $"Unauthorized:-{token.Message}",
                });
            }
            if (token.Role != Role.Admin)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    NotAuthenticated = true,
                    Message = "Sorry, you are not authorized to perform this action",
                });
            }
            var financialyears = _unisolApiProxy.GetFinancialYears().Result;
            var jdata = JsonConvert.DeserializeObject<ReturnData<List<string>>>(financialyears);
            return Json(jdata);
        }

        [HttpPost("GetStudentTerms")]
        public JsonResult GetStudentTerms(TermRequest request)
        {
            var token = _tokenValidator.Validate(HttpContext);
            if (!token.Success)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    NotAuthenticated = true,
                    Message = $"Unauthorized:-{token.Message}",
                });
            }
            if (token.Role != Role.Student)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    NotAuthenticated = true,
                    Message = "Sorry, you are not authorized to perform this action",
                });
            }
            var studTerms = _unisolApiProxy.GetStudentTerms(request.UserCode).Result;
            var jdata = JsonConvert.DeserializeObject<ReturnData<List<string>>>(studTerms);
            return Json(jdata);
        }

       public class TermRequest { 
            public string UserCode { get; set; }
        }
    }
}
