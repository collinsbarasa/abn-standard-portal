﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.ViewModels.Profile;
using Unisol.Web.Entities.Database.MembershipModels;
using Unisol.Web.Entities.Database.UnisolModels;
using Unisol.Web.Entities.Model;
using Unisol.Web.Portal.IRepository;
using Unisol.Web.Portal.Models;
using Unisol.Web.Portal.Utilities;

namespace Unisol.Web.Portal.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class WorkPlanController : Controller
    {
        private readonly PortalCoreContext _context;
        private readonly TokenValidator _tokenValidator;
        private readonly IWorkPlanRepository _workPlanRepository;
        
        public WorkPlanController(PortalCoreContext context, IWorkPlanRepository workPlanRepository)
        {
            _context = context;
            _workPlanRepository = workPlanRepository;
            _tokenValidator = new TokenValidator(_context);
        }

        [HttpPost("[action]")]
        public JsonResult CreateIndividualWorkPlan(IndividualWorkPlanVm model) {
            var result = _workPlanRepository.CreateIndividualWorkPlan(model.StaffNo, model.Id);
            return Json(new ReturnData<IndividualWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult CreateWorkPlan(DepartmentWorkPlan model)
        {
            var result = _workPlanRepository.CreateWorkPlan(model);
            return Json(new ReturnData<DepartmentWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult CreateWorkPlanObjective(WorkPlanObjective model)
        {
            var result = _workPlanRepository.CreateWorkPlanObjective(model);
            return Json(new ReturnData<WorkPlanObjective>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
        [HttpPost("[action]")]
        public JsonResult CreateWorkPlanActivity(WorkPlanActivityVm model)
        {
            var result = _workPlanRepository.CreateWorkPlanActivity(model);
            return Json(new ReturnData<WorkPlanActivity>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult CreateWorkPlanActivityEvaluation(WorkPlanActivityEvaluationVm model)
        {
            var result = _workPlanRepository.CreateWorkPlanActivityEvaluation(model);
            return Json(new ReturnData<WorkPlanActivityEvaluation>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult CreateWorkPlanActivityOfficers(WorkPlanActivityOfficerVm model)
        {
            var result = _workPlanRepository.CreateWorkplanActivityOfficers(model);
            return Json(new ReturnData<List<StaffProfileDataVm>>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult CreateWorkPlanActivityOutput(WorkPlanActivityOutputVm model)
        {
            var result = _workPlanRepository.CreateWorkPlanActivityOutput(model);
            return Json(new ReturnData<WorkPlanActivityOutput>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult DeleteWorkplanActivity(Guid id)
        {
            var result = _workPlanRepository.DeleteWorkPlanActivity(id);
            return Json(new ReturnData<WorkPlanActivity>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult DeleteWorkplanActivityEvaluation(Guid id)
        {
            var result = _workPlanRepository.DeleteWorkPlanActivityEvaluation(id);
            return Json(new ReturnData<WorkPlanActivityEvaluation>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult DeleteWorkPlanActivityOutput(Guid id)
        {
            var result = _workPlanRepository.DeleteWorkPlanActivityOutput(id);
            return Json(new ReturnData<WorkPlanActivityOutput>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult EditIndividualWorkPlan(Guid id)
        {
            var result = _workPlanRepository.EditIndividualWorkPlan(id);
            return Json(new ReturnData<IndividualWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult EditWorkPlan(DepartmentWorkPlan model)
        {
            var result = _workPlanRepository.EditWorkPlan(model);
            return Json(new ReturnData<DepartmentWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult EditWorkPlanActivity(WorkPlanActivityVm model)
        {
            var result = _workPlanRepository.EditWorkPlanActivity(model);
            return Json(new ReturnData<WorkPlanActivity>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult EditWorkPlanObjective(WorkPlanObjective model)
        {
            var result = _workPlanRepository.EditWorkPlanObjective(model);
            return Json(new ReturnData<WorkPlanObjective>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult EditWorkPlanActivityEvaluation(WorkPlanActivityEvaluationVm model)
        {
            var result = _workPlanRepository.EditWorkPlanActivityEvaluation(model);
            return Json(new ReturnData<WorkPlanActivityEvaluation>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpPost("[action]")]
        public JsonResult EditWorkPlanActivityOutput(WorkPlanActivityOutputVm model)
        {
            var result = _workPlanRepository.EditWorkPlanActivityOutput(model);
            return Json(new ReturnData<WorkPlanActivityOutput>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult GetIndividualWorkPlans(string financialYear = null, string department = null)
        {
            var result = _workPlanRepository.GetIndividualWorkPlans(financialYear, department);
            return Json(new ReturnData<List<IndividualWorkPlan>>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult GetStaffWorkPlans(string staffNo)
        {
            var result = _workPlanRepository.GetIndividualWorkPlans(staffNo);
            return Json(new ReturnData<List<IndividualWorkPlan>>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult GetIndividualWorkPlan(Guid id)
        {
            var result = _workPlanRepository.GetIndividualWorkPlan(id);
            return Json(new ReturnData<IndividualWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult GetWorkPlanObjective(Guid id)
        {
            var result = _workPlanRepository.GetWorkPlanObjective(id);
            return Json(new ReturnData<WorkPlanObjective>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
        [HttpGet("[action]")]
        public JsonResult GetWorkPlan(Guid id)
        {
            var result = _workPlanRepository.GetWorkPlan(id);
            return Json(new ReturnData<DepartmentWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult GetWorkPlans(string financialYear = null, string department = null)
        {
            var result = _workPlanRepository.GetWorkPlans(financialYear, department);
            return Json(new ReturnData<List<DepartmentWorkPlan>>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }

        [HttpGet("[action]")]
        public JsonResult Publish(Guid id , string action)
        {
            var result = _workPlanRepository.Publish(id, action);
            return Json(new ReturnData<bool>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
        [HttpGet("[action]")]
        public JsonResult Activate(Guid id, string action)
        {
            var result = _workPlanRepository.Activate(id, action);
            return Json(new ReturnData<bool>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
        [HttpGet("[action]")]
        public JsonResult RequestApproval(Guid id, string action)
        {
            var result = _workPlanRepository.RequestApproval(id, action);
            return Json(new ReturnData<IndividualWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
        [HttpGet("[action]")]
        public JsonResult Approve(Guid id, string action)
        {
            var result = _workPlanRepository.RequestApproval(id, action);
            return Json(new ReturnData<IndividualWorkPlan>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
        [HttpGet("[action]")]
        public JsonResult RemoveWorkPlanActivityOfficer(Guid id)
        {
            var result = _workPlanRepository.RemoveWorkPlanActivityOfficer(id);
            return Json(new ReturnData<HrpEmployee>
            {
                Success = result.Success,
                Message = result.Message,
                Data = result.Data
            });
        }
    }
}
