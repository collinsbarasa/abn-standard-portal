﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.ViewModels.Login;
using Unisol.Web.Common.ViewModels.Messages;
using Unisol.Web.Common.ViewModels.Settings;
using Unisol.Web.Entities.Database.MembershipModels;
using Unisol.Web.Entities.Model;
using Unisol.Web.Portal.IServices;
using Unisol.Web.Portal.Utilities;

namespace Unisol.Web.Portal.Services
{
	public class CroneJobs : ICroneJobs
	{
		private PortalCoreContext _context;
		private readonly IUnisolApiProxy _unisolApiProxy;
		private EmailSender emailSender;
		private string classStatus;
		private IHostingEnvironment _env;
		private IConfiguration _configuration;
		public CroneJobs(PortalCoreContext context, IUnisolApiProxy unisolApiProxy, IEmailService emailService, IHostingEnvironment env, IEmailConfiguration emailConfiguration, IConfiguration configuration)
		{
			_env = env;
			_context = context;
			_unisolApiProxy = unisolApiProxy;
			_configuration = configuration;
			classStatus = _context.Settings.FirstOrDefault()?.ClassStatus;
			emailSender = new EmailSender(configuration, context, emailConfiguration, env, emailService);
		}

		public void ClearHostel()
		{
			var settings = _context.Settings.FirstOrDefault();
			var bookingEndTime = settings.HostelBookingStartDate.GetValueOrDefault().AddHours(settings.HostelDuration);
			if (bookingEndTime < DateTime.UtcNow.AddHours(3))
				_unisolApiProxy.DeallocateHostel(classStatus);
		}

		public void PingPortals()
		{
			var clientIps = ClientIpValues.GetClientIps();
			var clientIp = new ClientIp();
			Ping pinger = new Ping();
			try
			{
				clientIps.ForEach(p => {
					clientIp = p;
					var ip = _context.ClientIp.FirstOrDefault(c => c.Ip.ToLower().Equals(clientIp.Ip.ToLower()));
					PingReply reply = pinger.Send(p.Ip);
					ip.Fails = 0;
				});
				_context.SaveChanges();
			}
			catch (PingException)
			{
				var ip = _context.ClientIp.FirstOrDefault(c => c.Ip.ToLower().Equals(clientIp.Ip.ToLower()));
				if(ip.Fails < 2)
				{
					ip.Fails = ip.Fails + 1;
				}
				else
				{
					ip.Fails = 0;
					//NotifyPortalDown(ip);
				}
				_context.SaveChanges();
			}
			pinger.Dispose();
		}

		private void NotifyPortalDown(ClientIp ip)
		{
			var mailReceiver = "womwitsa@abnosoftwares.com";
			try
			{
				var emailContent = new MailsViewModel
				{
					Code = ip.Institution,
					Email = mailReceiver,
					MailMethod = MailSendMethod.PortalDown,
					Subject = $"{ip.Institution} portal is inaccessible",
					PortalUrl = ip.Ip
				};

				var emailResponse = emailSender.SendEmail(emailContent);
				_context.InaccessibiltyLog.Add(new InaccessibiltyLog
				{
					Ip = ip.Ip,
					Institution = ip.Institution,
					MailReceiver = mailReceiver,
					DateCreated = DateTime.UtcNow.AddHours(3)
				});
			}
			catch (Exception)
			{
				
			}
		}

        public void SendNotification()
        {
			try
			{
				var settings = _context.Settings.FirstOrDefault();
				if (settings.AllowNotification)
                {
					var notifications = _context.Notification.Where(n => !n.IsFinalStatus).Include(n => n.Approvers).ToList();
					var result = _unisolApiProxy.GetNotificationRecipents(notifications).Result;
					var recipientResponse = JsonConvert.DeserializeObject<ReturnData<List<NotificationRecipientVm>>>(result);
					if (recipientResponse != null && recipientResponse.Success)
                    {
						foreach (var recipient in recipientResponse.Data)
						{
							recipient.IsFinalStatus = recipient?.IsFinalStatus ?? "";
							var notification = _context.Notification.FirstOrDefault(t => t.DocNo.ToUpper().Equals(recipient.DocNo.ToUpper()));
							var approvers = new List<Approver>();
							var dateCreated = DateTime.UtcNow.AddHours(3);
							if (notification != null)
							{
								dateCreated = notification.DateCreated.GetValueOrDefault();
								approvers = _context.Approver.Where(a => a.NotificationId == notification.Id).ToList();
								
								_context.Approver.RemoveRange(approvers);
								_context.Notification.Remove(notification);
							}

							var myApprover = new Approver
							{
								Id = Guid.NewGuid(),
								EmpNo = recipient.Approver,
								UserCode = recipient.ApproverUserCode,
								Level = recipient.ApproverLevel,
								Title = recipient.ApproverTitle,
							};

							if (!string.IsNullOrEmpty(recipient.TaskAction) && !recipient.TaskNotification)
								myApprover.TaskNotification = true;

							approvers.Add(myApprover);

							var isPending = recipient.IsFinalStatus.ToLower().Equals("pending") || string.IsNullOrEmpty(recipient.IsFinalStatus);
							var message = $"Dear {recipient.ApproverNames}, Kindly approve the {recipient.DocType} for {recipient.Names} ({recipient.UserCode}).";
							if (!string.IsNullOrEmpty(recipient.TaskAction))
								message = $"Dear {recipient.ApproverNames}, You have requested {recipient.Names} ({recipient.UserCode}) to {recipient.TaskAction} pending {recipient.DocType}";
							notification = new Notification
							{
								Id = Guid.NewGuid(),
								DocNo = recipient.DocNo,
								Status = recipient.IsFinalStatus,
								IsFinalStatus = !isPending,
								Content = message,
								Department = recipient.Department,
								DateCreated = dateCreated,
								DateModified = DateTime.UtcNow.AddHours(3),
								UserCode = recipient.UserCode,
								Approvers = approvers
							};

							var portalUrl = _configuration["Urls:UnisolPortalUrl"];
							var approver = new MailsViewModel
							{
								UserCode = recipient?.Approver ?? "",
								Firstname = recipient?.ApproverNames ?? "",
								Code = message,
								Email = recipient?.ApproverEmail ?? "",
								MailMethod = MailSendMethod.Notification,
								PortalUrl = portalUrl,
								Subject = recipient?.DocType ?? ""
							};

							if (!recipient.TaskNotification)
                            {
								var success = emailSender.SendEmail(approver);

								message = $"Dear {recipient.Names}, Your application for {recipient.DocType} is {recipient.Status} at {recipient.ApproverTitle}";
								if (!string.IsNullOrEmpty(recipient.TaskAction))
									message = $"Dear {recipient.Names}, {recipient.ApproverTitle} ({recipient.ApproverNames}) has requested you to {recipient.TaskAction} pending {recipient.DocType}";
								var requestor = new MailsViewModel
								{
									UserCode = recipient?.UserCode ?? "",
									Firstname = recipient?.Names ?? "",
									Code = message,
									Email = recipient?.Email ?? "",
									MailMethod = MailSendMethod.Notification,
									PortalUrl = portalUrl,
									Subject = recipient?.DocType ?? ""
								};
								success = emailSender.SendEmail(requestor);
								if (success)
								{
									_context.Notification.Add(notification);
									_context.SaveChanges();
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{

			}
		}
	}
}
