﻿using System;
using System.Collections.Generic;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.ViewModels.Profile;
using Unisol.Web.Entities.Database.MembershipModels;
using Unisol.Web.Entities.Database.UnisolModels;
using Unisol.Web.Entities.Model;

namespace Unisol.Web.Portal.IRepository
{
    public interface IWorkPlanRepository
    {
        ReturnData<List<DepartmentWorkPlan>> GetWorkPlans(string financialYear = null, string department = null);
        ReturnData<List<IndividualWorkPlan>> GetIndividualWorkPlans(string financialYear = null, string department = null);
        ReturnData<List<IndividualWorkPlan>> GetIndividualWorkPlans(string staffNo);
        ReturnData<DepartmentWorkPlan> GetWorkPlan(Guid id);
        ReturnData<WorkPlanObjective> GetWorkPlanObjective(Guid id);
        ReturnData<WorkPlanActivity> GetWorkPlanActivity(Guid id);
        ReturnData<bool> Activate(Guid id, string action);
        ReturnData<bool> Publish(Guid id, string action);
        ReturnData<IndividualWorkPlan> Approve(Guid id, string action);
        ReturnData<IndividualWorkPlan> GetIndividualWorkPlan(Guid id);
        ReturnData<IndividualWorkPlan> RequestApproval(Guid id, string action);
        ReturnData<DepartmentWorkPlan> CreateWorkPlan(DepartmentWorkPlan workPlan);
        ReturnData<DepartmentWorkPlan> EditWorkPlan(DepartmentWorkPlan workPlan);
        ReturnData<IndividualWorkPlan> CreateIndividualWorkPlan(string staffNo, Guid id);
        ReturnData<IndividualWorkPlan> EditIndividualWorkPlan(Guid id);
        ReturnData<WorkPlanActivity> CreateWorkPlanActivity(WorkPlanActivityVm model);
        ReturnData<WorkPlanObjective> CreateWorkPlanObjective(WorkPlanObjective model);
        ReturnData<WorkPlanActivity> EditWorkPlanActivity(WorkPlanActivityVm model);
        ReturnData<WorkPlanObjective> EditWorkPlanObjective(WorkPlanObjective model);
        ReturnData<WorkPlanActivity> DeleteWorkPlanActivity(Guid id);
        ReturnData<List<StaffProfileDataVm>> CreateWorkplanActivityOfficers(WorkPlanActivityOfficerVm model);
        ReturnData<HrpEmployee> RemoveWorkPlanActivityOfficer(Guid id);
        ReturnData<WorkPlanActivityOutput> CreateWorkPlanActivityOutput(WorkPlanActivityOutputVm model);
        ReturnData<WorkPlanActivityOutput> EditWorkPlanActivityOutput(WorkPlanActivityOutputVm model);
        ReturnData<WorkPlanActivityOutput> DeleteWorkPlanActivityOutput(Guid id);
        ReturnData<WorkPlanActivityEvaluation> CreateWorkPlanActivityEvaluation(WorkPlanActivityEvaluationVm model);
        ReturnData<WorkPlanActivityEvaluation> EditWorkPlanActivityEvaluation(WorkPlanActivityEvaluationVm model);
        ReturnData<WorkPlanActivityEvaluation> DeleteWorkPlanActivityEvaluation(Guid id);
        ReturnData<WorkPlanObjective> DeleteWorkPlanObjective(Guid id);
    }
}
