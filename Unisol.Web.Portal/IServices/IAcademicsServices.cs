﻿using System.Collections.Generic;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.ViewModels.Academics;

namespace Unisol.Web.Portal.IServices
{
	public interface IAcademicsServices
	{
		ReturnData<List<ExamCardUnitVm>> GetStudentExamCard(string userCode, bool isPreviousTermCard);
		ReturnData<List<ExamCardUnitVm>> GetSetStudentExamCard(string userCode, string termName);
	}
}
