﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Login
{
    public class LoginViewModel
    {
        public string Username { set; get; }
        public string Password { set; get; }
    }
	
	public enum MailSendMethod
	{
		AccountConfirmation = 1,
		PasswordReset = 2,
		Notification = 3,
		PortalDown = 4,
		
	}
}
