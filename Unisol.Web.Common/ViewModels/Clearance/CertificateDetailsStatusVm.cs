﻿using Unisol.Web.Common.ViewModels.Institution;

namespace Unisol.Web.Common.ViewModels.Clearance
{
   public class CertificateDetailsStatusVm
    {
        public CertificateDataVm UsersDetails { get; set; }
        public string CertificateType { get; set; }
    }
}
