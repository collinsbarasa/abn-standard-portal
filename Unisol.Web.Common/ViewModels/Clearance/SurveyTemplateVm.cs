﻿using System.Collections.Generic;
using Unisol.Web.Entities.Database.MembershipModels;

namespace Unisol.Web.Common.ViewModels.Clearance
{
    public class SurveyTemplateVm
    {
        public List<ClearanceQuestionnaireTemplate> SurveyTemplates { get; set; }
        public List<ClearanceSurvey> Surveys { get; set; }
        public string[] SurveyStatus { get; set; }
    }
}
