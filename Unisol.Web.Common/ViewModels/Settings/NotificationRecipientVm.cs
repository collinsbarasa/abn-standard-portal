﻿namespace Unisol.Web.Common.ViewModels.Settings
{
    public class NotificationRecipientVm
    {
        public string DocNo { get; set; }
        public string Status { get; set; }
        public string IsFinalStatus { get; set; }
        public string UserCode { get; set; }
        public string Approver { get; set; }
        public string Email { get; set; }
        public string ApproverEmail { get; set; }
        public string ApproverTitle { get; set; }
        public string DocType { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public string Names { get; set; }
        public string ApproverUserCode { get; set; }
        public int ApproverLevel { get; set; }
        public string ApproverNames { get; set; }
        public string TaskAction { get; set; }
        public bool TaskNotification { get; set; }
    }
}
