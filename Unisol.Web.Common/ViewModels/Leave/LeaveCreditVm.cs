﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Leave
{
    public class LeaveCreditVm
    {
        public List<LeaveDaysSum> Leave { get; set; }
        public List<EmpNoNames> Employees { get; set; }
    }

    public class EmpNoNames { 
        public string EmpNo { get; set; }
        public string Names { get; set; }
    }
}
