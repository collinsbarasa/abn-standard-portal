﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Leave
{
   public class HrpLeaveAppVm
    {
        public string Ref { get; set; }
        public int LeavePeriod { get; set; }
        public string EmpNo { get; set; }
        public string LeaveType { get; set; }
        public DateTime? Sdate { get; set; }
        public string Stime { get; set; }
        public DateTime? Edate { get; set; }
        public string Etime { get; set; }
        public decimal? LeaveDays { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public DateTime? Rdate { get; set; }
        public string Personnel { get; set; }
        public string Reliever { get; set; }
        public bool? Emergency { get; set; }
    }
}
