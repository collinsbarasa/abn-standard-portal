﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Payslip
{
    public class SalaryPeriodVm
    {
        public string Names { get; set; }
        public int Id { get; set; }
    }
}
