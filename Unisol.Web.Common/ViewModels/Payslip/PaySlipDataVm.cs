﻿

using System;

namespace Unisol.Web.Common.ViewModels.Payslip
{
    public class PaySlipDataVm
    {
        public PaymentPerDeductionEarning SortedPayments { get; set; }
        public EarningDeductions EarningDeductions { get; set; }
        public DateTime? RDate { get; set; }
    }
}
