﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.loans
{
   public class UserInfoVm
    {
        public string EmpNo { get; set; }
        public string Names { get; set; }
        public string Idno { get; set; }
        public string AccNo { get; set; }
    }
}
