﻿using System;
using System.Collections.Generic;
using System.Text;
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Common.ViewModels.loans
{
   public class LoansListDataVm
    {
        public List<HrpLoanSetup> LoansData { get; set; }
        public UserInfoVm UserInfoVm { get; set; }
    }
}
