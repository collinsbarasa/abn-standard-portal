﻿
namespace Unisol.Web.Common.ViewModels.Institution
{
    public class InstitutionVm
    {
        public string OrgName { get; set; }
        public string SubTitle { get; set; }
        public string Contacts { get; set; }
        public string EmailUname { get; set; }
        public string Smtpserver { get; set; }
    }
}
