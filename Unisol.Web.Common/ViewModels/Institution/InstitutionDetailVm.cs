﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Institution
{
    public class InstitutionDetailVm
    {
        public InstitutionVm Setting { get; set; }
        public string[] ContactArray { get; set; }
    }
}
