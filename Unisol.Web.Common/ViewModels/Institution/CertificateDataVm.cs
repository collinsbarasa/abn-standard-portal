﻿
namespace Unisol.Web.Common.ViewModels.Institution
{
   public class CertificateDataVm
    {
        public CertificateVm CertificateDetails { get;set; }
        public bool HasCleared { get; set; }
    }
}
