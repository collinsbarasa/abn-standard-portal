﻿
namespace Unisol.Web.Common.ViewModels.Profile
{
    public class StaffProfileDataVm
    {
        public string Names {get;set;}
        public string EmpNo {get;set;}
        public string Pin {get;set;}
        public string Idno {get;set;}
        public string Department {get;set;}
        public string Title {get;set;}
        public string Nhif {get;set;}
        public string Nssf {get;set;}
		public string Supervisor {get;set;}
        public string JobCat {get;set;}
        public string JobTitle {get;set;}
        public string SupervisorName {get;set;}
        public string Pgrade  {get;set;}
        public string Bank {get;set;}
        public string LeaveGroup {get;set;}
        public string Cell {get;set;}
        public string County {get;set;}
        public string County2 {get;set;}
        public string Country {get;set;}
        public string Address {get;set;}
        public string City {get;set;}
        public string Pemail {get;set;}
        public string Wemail {get;set;}
        public string EmpCat { get; set; }
    }
}
