﻿
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Common.ViewModels.Profile
{
    public class StudentProfileDataVm
    {
        public Register Register { get; set; }
        public StudDependant Dependants { get; set; }
    }
}
