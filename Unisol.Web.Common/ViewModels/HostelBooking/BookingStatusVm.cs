﻿

namespace Unisol.Web.Common.ViewModels.HostelBooking
{
    public class BookingStatusVm
    {
        public bool Status { get; set; }

        public string CurrentTerm { get; set; }
        public BookingRateVm Data { get; set; }
    }
}
