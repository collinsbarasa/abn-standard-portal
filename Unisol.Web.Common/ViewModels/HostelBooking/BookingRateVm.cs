﻿

namespace Unisol.Web.Common.ViewModels.HostelBooking
{
    public class BookingRateVm
    {
        public int Rate { get; set; }
		public int YearIndex { get; set; }
		public string InistitutionInitial { get; set; }
    }
}
