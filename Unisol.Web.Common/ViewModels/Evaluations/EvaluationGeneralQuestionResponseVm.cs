﻿using System;

namespace Unisol.Web.Common.ViewModels.Evaluations
{
    public class EvaluationGeneralQuestionResponseVm
    {
        public string UserNo { get; set; }
        public string Name { get; set; }
        public string Answer { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
