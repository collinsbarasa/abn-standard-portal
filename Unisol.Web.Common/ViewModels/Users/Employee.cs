﻿
namespace Unisol.Web.Common.ViewModels.Users
{
    public class Employee
    {
        public string Names { get; set; }
        public string StaffNo { get; set; }
        public string Email { get; set; }
    }
}
