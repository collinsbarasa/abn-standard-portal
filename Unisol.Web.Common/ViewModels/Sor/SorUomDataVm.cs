﻿using System.Collections.Generic;
using Unisol.Web.Common.ViewModel.Sor;

namespace Unisol.Web.Common.ViewModels.Sor
{
    public class SorUomDataVm
    {
        public List<SorDataVm> Sors { get; set; }
        public List<string> Uom { get; set; }
    }
}
