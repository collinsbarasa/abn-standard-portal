﻿
using System.Collections.Generic;

namespace Unisol.Web.Common.ViewModels.Sor
{
    public class IRProcDataVm
    {
        public List<ProcItemDataVm> ProcItems { get; set; }
        public List<IRDataVm> Requisitions { get; set; }
    }
}
