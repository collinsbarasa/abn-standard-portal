﻿
namespace Unisol.Web.Common.ViewModels.Sor
{
    public class IRItemModel
    {
        public int Id { get; set; }
        public string Quantity { get; set; }
        public string Unitmeasure { get; set; }
        public decimal Unitamount { get; set; }
        public decimal Totalamount { get; set; }
        public string Description { get; set; }
    }
}
