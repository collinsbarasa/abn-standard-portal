﻿

namespace Unisol.Web.Common.ViewModels.Sor
{
   public class SorItemModel
    {
        public int Id { get; set; }
        public string Quantity { get; set; }
        public string Unitmeasure { get; set; }
        public string Unitamount { get; set; }
        public decimal Totalamount { get; set; }
        public string Description { get; set; }
    }
}
