﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Sor
{
    public class ProcItemDataVm
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Uom { get; set; }
        public decimal? Cost { get; set; }
    }
}
