﻿

using System.Collections.Generic;

namespace Unisol.Web.Common.ViewModels.Sor
{
    public class CreateIRModel
    {
        public SorDetailsModel Details { get; set; }
        public List<IRItemModel> Items { get; set; }
    }
}
