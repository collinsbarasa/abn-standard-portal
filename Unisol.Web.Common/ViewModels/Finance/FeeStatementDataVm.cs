﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Finance
{
    public class FeeStatementDataVm
    {
        public TotalDebitCreditViewModel Totals { get; set; }
        public List<StatementVm> Statement { get; set; }
    }
}
