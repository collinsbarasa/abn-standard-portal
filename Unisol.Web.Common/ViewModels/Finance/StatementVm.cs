﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Finance
{
   public class StatementVm
    {
		public string AdmnNo { get; set; }
		public string Term { get; set; }
		public string Rdate { get; set; }
		public string Type { get; set; }
		public string Ref { get; set; }
		public string Description { get; set; }
		public string Debit { get; set; }
		public string Credit { get; set; }
		public string Balance { get; set; }
		public string Notes { get; set; }
	}
}
