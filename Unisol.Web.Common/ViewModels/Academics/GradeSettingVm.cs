﻿
using System.Collections.Generic;
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Common.ViewModels.Academics
{
    public class GradeSettingVm
    {
        public List<Grading> Grading { get; set; }
        public List<MarkSymbols> MarkSymbols { get; set; }
    }
}
