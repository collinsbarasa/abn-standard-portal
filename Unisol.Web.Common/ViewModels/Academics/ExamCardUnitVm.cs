﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Academics
{
   public class ExamCardUnitVm
    {
        public string Term { get; set; }
        public string Status { get; set; }
        public string UnitCode { get; set; }
        public string AdmnNo { get; set; }
        public string Names { get; set; }
        public decimal? CreditUnits { get; set; }
    }
}
