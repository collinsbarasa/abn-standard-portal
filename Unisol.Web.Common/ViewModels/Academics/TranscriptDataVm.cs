﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unisol.Web.Common.ViewModels.Academics
{
    public class TranscriptDataVm
    {
		public List<YearTranscriptViewModel> Results { get; set; }
		public decimal Average { get; set; }
		public decimal CumulativeAverage { get; set; }
		public string Remarks { get; set; }
		public GradeSettingVm GradeSettings { get; set; }
		public string InstitutionInitials { get; set; }
		public string TranscriptNote { get; set; }
	}
}
