﻿

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class WorkPlanActivityOutput : BaseTable
    {
        public string Description { get; set; }
    }
}
