﻿using System;
using System.Collections.Generic;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class WorkPlanObjective : BaseTable
    {
        public string Description { get; set; }
        public Guid WorkPlanId { get; set; }
        public List<WorkPlanActivity> WorkPlanActivities { get; set; }
    }
}
