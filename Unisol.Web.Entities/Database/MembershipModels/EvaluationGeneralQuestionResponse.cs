﻿using System;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class EvaluationGeneralQuestionResponse
    {
        public int Id { get; set; }
        public int EvaluationId { get; set; }
        public string Response { get; set; }
        public string UserNo { get; set; }
        public DateTime DateAdded { get; set; } = DateTime.Now;
    }
}
