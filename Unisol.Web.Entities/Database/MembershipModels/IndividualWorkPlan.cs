﻿
using System;
using System.Collections.Generic;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class IndividualWorkPlan : WorkPlanBase
    {
        public IndividualWorkPlan() { }
        public IndividualWorkPlan(WorkPlanBase plan) {
            Id = Guid.NewGuid();
            Department = plan.Department;
            OfficerInCharge = plan.OfficerInCharge;
            FinancialYear = plan.FinancialYear;
            Status = "Created";
            WorkPlanObjectives = InitializeObjectives(plan.WorkPlanObjectives);
        }
        public string StaffNo { get; set; }

        private List<WorkPlanObjective> InitializeObjectives(List<WorkPlanObjective> objectives) {
            var list = new List<WorkPlanObjective>();
            if (objectives != null) {
                objectives.ForEach(e => {
                    var obj = new WorkPlanObjective{ 
                    Id = Guid.NewGuid(),
                    Description = e.Description,
                    WorkPlanActivities = new List<WorkPlanActivity>(),
                    };
                    list.Add(obj);
                });
            }

            return list;
        }
    }
}
