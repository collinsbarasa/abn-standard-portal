﻿using System.Collections.Generic;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class WorkPlanBase : BaseTable
    {
        public string FinancialYear { get; set; }
        public string Department { get; set; }
        public string SectionUnit { get; set; }
        public string OfficerInCharge { get; set; }
        public string Status { get; set; }
        public List<WorkPlanObjective> WorkPlanObjectives { get; set; }
    }
}
