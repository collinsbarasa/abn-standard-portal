﻿using System;

namespace Unisol.Web.Entities.Database.MembershipModels
{
	public class ClearanceSurvey
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string TempleteName { get; set; }
		public DateTime CreatedDate { get; set; }
		public DateTime? StartTime { get; set; }
		public DateTime? EndTime { get; set; }
        public Role? Role { get; set; }
        //public string Status { get; set; }
    }
}
