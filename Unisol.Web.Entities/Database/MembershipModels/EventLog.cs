﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class EventLog
    {
        public int Id { get; set; }
        public string UserCode { get; set; }
        public string Event { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
