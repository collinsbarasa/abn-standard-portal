﻿using System;
using System.Collections.Generic;
using Unisol.Web.Entities.Model;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class WorkPlanActivity : BaseTable
    {
        public WorkPlanActivity() { }
        public WorkPlanActivity(WorkPlanActivityVm model) {
            Budget = Convert.ToDouble(model.Budget);
            MonitoringEvaluations = new List<WorkPlanActivityEvaluation>();
            Description = model.Description;
            Quarter = model.Quarter;
            ByDate = model.ByDate;
            Officers = new List<WorkPlanActivityOfficer>();
            OutPuts = new List<WorkPlanActivityOutput>();
            Status = "Pending";
        }
        public string Description { get; set; }
        public string Quarter { get; set; }
        public DateTime ByDate { get; set; }
        public List<WorkPlanActivityOfficer> Officers { get; set; }
        public List<WorkPlanActivityOutput> OutPuts { get; set; }
        public List<WorkPlanActivityEvaluation> MonitoringEvaluations { get; set; }
        public double Budget { get; set; }
        public string Status { get; set; }
    }
}
