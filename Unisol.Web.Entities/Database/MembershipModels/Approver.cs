﻿using System;

namespace Unisol.Web.Entities.Database.MembershipModels
{
    public class Approver
    {
        public Guid Id { get; set; }
        public Guid? NotificationId { get; set; }
        public string EmpNo { get; set; }
        public string UserCode { get; set; }
        public int Level { get; set; }
        public string Title { get; set; }
        public string Action { get; set; }
        public bool TaskNotification { get; set; }
    }
}
