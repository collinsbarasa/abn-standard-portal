﻿using System;

namespace Unisol.Web.Entities.Database.UnisolModels
{
    public partial class WFDocCentreTasks
    {
        public int ID { get; set; }
        public string Ref { get; set; }
        public string Task { get; set; }
        public string Status { get; set; }
        public string RaisedBy { get; set; }
        public DateTime? RaisedDate { get; set; }
        public DateTime? RaisedTime { get; set; }
        public string ActionedBy { get; set; }
        public DateTime? ActionedDate { get; set; }
        public DateTime? ActionedTime { get; set; }
    }
}
