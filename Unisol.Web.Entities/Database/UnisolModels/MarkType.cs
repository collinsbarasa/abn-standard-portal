﻿
using System.ComponentModel.DataAnnotations;

namespace Unisol.Web.Entities.Database.UnisolModels
{
    public partial class MarkType
    {
        [Key]
        public string Names { get; set; }
        public bool Closed { get; set; }
        public string Notes { get; set; }
    }
}
