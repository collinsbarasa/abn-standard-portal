﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Entities.Model
{
    public class ClassStatusVm
    {
        public List<ClassStatus> ClassStatus { get; set; }
        public string InstitutionInitial { get; set; }
    }
}
