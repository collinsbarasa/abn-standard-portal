﻿using System;
using Unisol.Web.Entities.Database.MembershipModels;

namespace Unisol.Web.Entities.Model
{
    public class WorkPlanActivityEvaluationVm : WorkPlanActivityEvaluation
    {
        public Guid ActivityId { get; set; }
    }
}
