﻿using System;
using Unisol.Web.Entities.Database.MembershipModels;

namespace Unisol.Web.Entities.Model
{
    public class WorkPlanActivityOutputVm : WorkPlanActivityOutput
    {
        public Guid ActivityId { get; set; }
    }
}
