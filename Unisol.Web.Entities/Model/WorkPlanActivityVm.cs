﻿using System;
using System.Collections.Generic;
using Unisol.Web.Entities.Database.MembershipModels;

namespace Unisol.Web.Entities.Model
{
    public class WorkPlanActivityVm 
    {
        public Guid ObjectiveId { get; set; }
        public string Description { get; set; }
        public string Quarter { get; set; }
        public DateTime ByDate { get; set; }
        public List<WorkPlanActivityOfficer> Officers { get; set; }
        public List<WorkPlanActivityOutput> OutPuts { get; set; }
        public List<WorkPlanActivityEvaluation> MonitoringEvaluations { get; set; }
        public string Budget { get; set; }
        public string Status { get; set; }
    }
}
