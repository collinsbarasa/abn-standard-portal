﻿using System;
using System.Collections.Generic;

namespace Unisol.Web.Entities.Model
{
    public class WorkPlanActivityOfficerVm
    {
        public Guid ActivityId { get; set; }
        public List<string> StaffNumbers { get; set; }
    }
}
