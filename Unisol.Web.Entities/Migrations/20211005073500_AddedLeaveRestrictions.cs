﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Unisol.Web.Entities.Migrations
{
    public partial class AddedLeaveRestrictions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxEmergencyLeave",
                table: "Settings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinLeaveApplicationNotice",
                table: "Settings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MinMaternityApplicationNotice",
                table: "Settings",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxEmergencyLeave",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "MinLeaveApplicationNotice",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "MinMaternityApplicationNotice",
                table: "Settings");
        }
    }
}
