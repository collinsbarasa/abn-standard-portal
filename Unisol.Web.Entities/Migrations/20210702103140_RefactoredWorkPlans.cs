﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Unisol.Web.Entities.Migrations
{
    public partial class RefactoredWorkPlans : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkPlanObjectives_WorkPlans_WorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropIndex(
                name: "IX_WorkPlanObjectives_WorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropPrimaryKey(
                name: "PK_WorkPlans",
                table: "WorkPlans");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "WorkPlans");

            migrationBuilder.DropColumn(
                name: "Published",
                table: "WorkPlans");

            migrationBuilder.RenameTable(
                name: "WorkPlans",
                newName: "IndividualWorkPlans");

            migrationBuilder.AddColumn<Guid>(
                name: "DepartmentWorkPlanId",
                table: "WorkPlanObjectives",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "IndividualWorkPlanId",
                table: "WorkPlanObjectives",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_IndividualWorkPlans",
                table: "IndividualWorkPlans",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "DepartmentWorkPlans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    FinancialYear = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    SectionUnit = table.Column<string>(nullable: true),
                    OfficerInCharge = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Published = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentWorkPlans", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanObjectives_DepartmentWorkPlanId",
                table: "WorkPlanObjectives",
                column: "DepartmentWorkPlanId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanObjectives_IndividualWorkPlanId",
                table: "WorkPlanObjectives",
                column: "IndividualWorkPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkPlanObjectives_DepartmentWorkPlans_DepartmentWorkPlanId",
                table: "WorkPlanObjectives",
                column: "DepartmentWorkPlanId",
                principalTable: "DepartmentWorkPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkPlanObjectives_IndividualWorkPlans_IndividualWorkPlanId",
                table: "WorkPlanObjectives",
                column: "IndividualWorkPlanId",
                principalTable: "IndividualWorkPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkPlanObjectives_DepartmentWorkPlans_DepartmentWorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkPlanObjectives_IndividualWorkPlans_IndividualWorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropTable(
                name: "DepartmentWorkPlans");

            migrationBuilder.DropIndex(
                name: "IX_WorkPlanObjectives_DepartmentWorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropIndex(
                name: "IX_WorkPlanObjectives_IndividualWorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropPrimaryKey(
                name: "PK_IndividualWorkPlans",
                table: "IndividualWorkPlans");

            migrationBuilder.DropColumn(
                name: "DepartmentWorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.DropColumn(
                name: "IndividualWorkPlanId",
                table: "WorkPlanObjectives");

            migrationBuilder.RenameTable(
                name: "IndividualWorkPlans",
                newName: "WorkPlans");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "WorkPlans",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "Published",
                table: "WorkPlans",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_WorkPlans",
                table: "WorkPlans",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanObjectives_WorkPlanId",
                table: "WorkPlanObjectives",
                column: "WorkPlanId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkPlanObjectives_WorkPlans_WorkPlanId",
                table: "WorkPlanObjectives",
                column: "WorkPlanId",
                principalTable: "WorkPlans",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
