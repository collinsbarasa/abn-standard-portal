﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Unisol.Web.Entities.Migrations
{
    public partial class AddedStudentStatusRemarksInComplaint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StudentRemarks",
                table: "Complaint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StudentStatus",
                table: "Complaint",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StudentRemarks",
                table: "Complaint");

            migrationBuilder.DropColumn(
                name: "StudentStatus",
                table: "Complaint");
        }
    }
}
