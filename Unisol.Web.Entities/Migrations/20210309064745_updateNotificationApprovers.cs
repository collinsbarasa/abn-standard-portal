﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Unisol.Web.Entities.Migrations
{
    public partial class updateNotificationApprovers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TaskPending",
                table: "Notification");

            migrationBuilder.AddColumn<string>(
                name: "Action",
                table: "Approver",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TaskNotification",
                table: "Approver",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Action",
                table: "Approver");

            migrationBuilder.DropColumn(
                name: "TaskNotification",
                table: "Approver");

            migrationBuilder.AddColumn<bool>(
                name: "TaskPending",
                table: "Notification",
                nullable: false,
                defaultValue: false);
        }
    }
}
