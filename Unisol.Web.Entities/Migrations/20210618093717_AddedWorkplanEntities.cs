﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Unisol.Web.Entities.Migrations
{
    public partial class AddedWorkplanEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WorkPlans",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    FinancialYear = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    SectionUnit = table.Column<string>(nullable: true),
                    OfficerInCharge = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Published = table.Column<bool>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    StaffNo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkPlans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkPlanObjectives",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    WorkPlanId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkPlanObjectives", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkPlanObjectives_WorkPlans_WorkPlanId",
                        column: x => x.WorkPlanId,
                        principalTable: "WorkPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkPlanActivities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Quarter = table.Column<string>(nullable: true),
                    ByDate = table.Column<DateTime>(nullable: false),
                    Budget = table.Column<double>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    WorkPlanObjectiveId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkPlanActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkPlanActivities_WorkPlanObjectives_WorkPlanObjectiveId",
                        column: x => x.WorkPlanObjectiveId,
                        principalTable: "WorkPlanObjectives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkPlanActivityEvaluation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    WorkPlanActivityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkPlanActivityEvaluation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkPlanActivityEvaluation_WorkPlanActivities_WorkPlanActivityId",
                        column: x => x.WorkPlanActivityId,
                        principalTable: "WorkPlanActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkPlanActivityOfficer",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    StaffNo = table.Column<string>(nullable: true),
                    WorkPlanActivityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkPlanActivityOfficer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkPlanActivityOfficer_WorkPlanActivities_WorkPlanActivityId",
                        column: x => x.WorkPlanActivityId,
                        principalTable: "WorkPlanActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkPlanActivityOutput",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    WorkPlanActivityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkPlanActivityOutput", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkPlanActivityOutput_WorkPlanActivities_WorkPlanActivityId",
                        column: x => x.WorkPlanActivityId,
                        principalTable: "WorkPlanActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanActivities_WorkPlanObjectiveId",
                table: "WorkPlanActivities",
                column: "WorkPlanObjectiveId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanActivityEvaluation_WorkPlanActivityId",
                table: "WorkPlanActivityEvaluation",
                column: "WorkPlanActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanActivityOfficer_WorkPlanActivityId",
                table: "WorkPlanActivityOfficer",
                column: "WorkPlanActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanActivityOutput_WorkPlanActivityId",
                table: "WorkPlanActivityOutput",
                column: "WorkPlanActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkPlanObjectives_WorkPlanId",
                table: "WorkPlanObjectives",
                column: "WorkPlanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkPlanActivityEvaluation");

            migrationBuilder.DropTable(
                name: "WorkPlanActivityOfficer");

            migrationBuilder.DropTable(
                name: "WorkPlanActivityOutput");

            migrationBuilder.DropTable(
                name: "WorkPlanActivities");

            migrationBuilder.DropTable(
                name: "WorkPlanObjectives");

            migrationBuilder.DropTable(
                name: "WorkPlans");
        }
    }
}
