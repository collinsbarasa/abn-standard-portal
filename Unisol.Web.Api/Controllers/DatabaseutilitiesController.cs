﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Unisol.Web.Api.IServices;
using Unisol.Web.Api.StudentUtilities;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.ViewModels.Academics;
using Unisol.Web.Common.ViewModels.Institution;
using Unisol.Web.Common.ViewModels.Settings;
using Unisol.Web.Entities.Database.MembershipModels;
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Api.Controllers
{
	[Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DatabaseUtilitiesController : Controller
    {
        private readonly UnisolAPIdbContext _context;
		private IStudentServices _studentServices;
		private StudentCredential studentCredentials;
		public DatabaseUtilitiesController(UnisolAPIdbContext context, IStudentServices studentServices, ISystemServices systemServices)
        {
            _context = context;
			_studentServices = studentServices;
			studentCredentials = new StudentCredential(_context, studentServices, systemServices);
		}
	
        public string GetCurrectTerm(string usercode)
        {
            var userEnrollment = _context.StudEnrolment.FirstOrDefault(s => s.AdmnNo == usercode);
            if (userEnrollment == null)
                return "";

			var studentClass = userEnrollment.Class;
			var term = _context.Class
					.Join(_context.Term,
						studentClassJoin => studentClassJoin.Term,
						t => t.Type,
						(studentClassJoin, t) =>
							new { t.Names, t.Starts, t.Ends, t.TermAlias, t.Type, studentClassJoin.Id })
					.FirstOrDefault(c =>
						c.Id.CaseInsensitiveContains(studentClass) &&
						(c.Starts <= DateTime.Now.Date && c.Ends >= DateTime.Now.Date))
				;
			if (term == null)
				return "";

			return term.Names;
        }

        [HttpGet("[action]")]
        public JsonResult GetCurrectActiveTerm(string usercode, string classStatus)
        {
            try
            {
				var termResponse = _studentServices.GetCurrentTerm(usercode, classStatus);
				if (!termResponse.Success)
					return Json(termResponse);

                return Json(new ReturnData<string>
                {
                    Success = true,
                    Message = "Current Active Term",
                    Data = termResponse.Data?.Names
				});
            }
            catch (Exception ex)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    Message = "Oops!  something went wrong while retrieving the current term, please try again"
                });
            }
        }

        [HttpGet("GetReportStatus")]
        public JsonResult GetReportStatus(string userCode, string classStatus)
        {
			return Json(studentCredentials.ValidateSessionReporting(userCode, classStatus));
        }
       
        [HttpGet("GetStudentTerm")]
        public JsonResult GetStudentTerm(string usercode, string classStatus)
        {
			return Json(_studentServices.GetCurrentTerm(usercode, classStatus));
        }

        [HttpGet("GetTerms")]
        public JsonResult GetTerms()
        {
            var terms = _studentServices.GetTerms();
            return Json(new ReturnData<List<string>> { 
                Success = terms.Success,
                Data = terms.Data
            });
        }
      
        [HttpGet("[action]")]
        public JsonResult StudentsYearsBeenInSchool(string usercode, string classStatus)
        {
            try
            {
                var studentYears = studentCredentials.GetYearsStudentHasBeenToSchool(usercode, classStatus);
                return Json(studentYears);
            }
            catch (Exception ex)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    Message = "Oops!  something went wrong while retrieving details, please try again"
                });
            }
        }

        [HttpGet("[action]")]
        public JsonResult StudentSessionsWithMarks(string usercode, string classStatus)
        {
            try
            {
                var studentYears = studentCredentials.StudentSessionsWithMarks(usercode, classStatus);
                return Json(studentYears);
            }
            catch (Exception ex)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    Message = "Oops!  something went wrong while retrieving details, please try again"
                });
            }
        }

        [HttpGet("[action]")]
        public JsonResult GetFeesYears(string progCode, string classStatus)
        {
            try
            {
                var feeStructProg = _context.FeesPerProg.FirstOrDefault(f => f.ProgCode == progCode);
				if(feeStructProg == null)
					return Json(new ReturnData<List<YearWithSemesterViewModel>>
					{
						Success = false,
						Message = "Sorry, Your fee structure not found. Kindly contact admin",
					});

				var feeStructProgDetails = _context.FeesPerProgDetail.Where(f => f.Ref == "" + feeStructProg.Id).ToList();

                var academicYears = feeStructProgDetails
                    .GroupBy(d => d.Stage)
                    .Select(grp => grp.First())
                    .ToList();

                var classWithSemesters = new List<YearWithSemesterViewModel>();

                academicYears.ForEach(y =>
                {
                    var semesters = new List<StudentSemesterYear>();
                    feeStructProgDetails.ForEach(s =>
                    {
                        if (s.Stage == y.Stage)
                        {
                            semesters.Add(new StudentSemesterYear
                            {
                                Id = s.Ref,
                                YearOfStudy = s.Stage,
                                Ref = s.Ref,
                                Semester = s.Term
                            });
                        }
                    });


                    classWithSemesters.Add(
                        new YearWithSemesterViewModel
                        {
                            Academicyear = y.Stage,
                            Semesters = semesters
                        }
                    );
                });
                return Json(new ReturnData<List<YearWithSemesterViewModel>>
                {
                    Success = true,
                    Message = "",
                    Data = classWithSemesters
                });

            }
            catch (Exception ex)
            {
                return Json(new ReturnData<string>
                {
                    Success = false,
                    Message = "Oops!  something went wrong while retrieving data, please try again " +
                              ErrorMessangesHandler.ExceptionMessage(ex)
                });
            }
        }

        public string Field(string contacts, string search)
        {
            var contactArray = contacts.Split("\r\n");
			if (contactArray.Count() < 2)
				contactArray = contactArray[0].Split(":");


			foreach (var item in contactArray)
            {
                if (item.CaseInsensitiveContains(search))
                {
                    return item;
                }
            }
            return search.ToUpper() + " : ";
        }
        [HttpGet("[action]")]
        public dynamic GetInstitutionHeaderDetails()
        {
            try
            {
                var setting = _context.SysSetup.Select(s => new InstitutionVm
                {
                    OrgName = s.OrgName,
                    SubTitle = s.SubTitle,
                    Contacts = s.Contacts,
                    EmailUname = s.EmailUname,
                    Smtpserver = s.Smtpserver,
                }).FirstOrDefault();

				if (setting == null)
					return Json(new ReturnData<InstitutionDetailVm>
					{
						Success = false,
						Message = "Oops!  We could find any system settings"
					});

				dynamic institutionInfo = new InstitutionDetailVm {
					Setting = setting
				};

				if (!string.IsNullOrEmpty(setting.Contacts))
				{
					var contactArray = setting.Contacts.Split("\r\n");
					if (contactArray.Count() < 2)
						contactArray = contactArray[0].Split(":");

					institutionInfo = new InstitutionDetailVm
					{
						Setting = setting,
						ContactArray = contactArray
					};
				}

				return Json(new ReturnData<InstitutionDetailVm>
				{
					Success = true,
					Message = "",
					Data = institutionInfo
				});
            }
            catch (Exception ex)
            {
                return Json(new ReturnData<InstitutionDetailVm>
                {
                    Success = false,
                    Message = "Oops!  something went wrong while retrieving data, please try again ",
                    Error = new Error(ex)
                });
            }
        }

        [HttpPost("[action]")]
        public JsonResult GetNotificationRecipents(List<Notification> notifications)
        {
            try
            {
                var docs = notifications.Select(n => n.DocNo.ToUpper()).ToList();
                var docCentres = _context.WfdocCentre.Where(d => d.FinalStatus.ToLower().Equals("pending")
                || docs.Contains(d.DocNo.ToUpper())).ToList();
                var recipients = new List<NotificationRecipientVm>();
                docCentres.ForEach(d =>
                {
                    var notification = notifications.FirstOrDefault(n => n.DocNo.ToUpper().Equals(d.DocNo.ToUpper()));
                    if (notification == null)
                        notification = new Notification();
                    var approverResponse = GetApprover(notification.Approvers, d);
                    if (approverResponse.Success)
                    {
                        var requestor = _context.HrpEmployee.Select(e => new HrpEmployee {
                            EmpNo = e.EmpNo,
                            Wemail = e.Wemail,
                            Names = e.Names
                        }).FirstOrDefault(e => e.EmpNo.ToUpper().Equals(d.UserRef.ToUpper()));
                        if(requestor == null)
                            requestor = _context.Register.Select(r => new HrpEmployee {
                                Wemail = r.Email,
                                EmpNo = r.AdmnNo,
                                Names = r.Names
                            }).FirstOrDefault(e => e.EmpNo.ToUpper().Equals(d.UserRef.ToUpper()));

                        approverResponse.Data.EmpNo = approverResponse?.Data?.EmpNo ?? "";
                        var approver = _context.HrpEmployee.FirstOrDefault(e => e.EmpNo.ToUpper().Equals(approverResponse.Data.EmpNo.ToUpper()));
                        if (requestor != null && approver != null)
                            if (!string.IsNullOrEmpty(requestor.Wemail) && !string.IsNullOrEmpty(approver.Wemail))
                            {
                                recipients.Add(new NotificationRecipientVm
                                {
                                    DocNo = d.DocNo,
                                    Status = d?.LatestApprovalStatus ?? "Pending",
                                    IsFinalStatus = d.FinalStatus,
                                    UserCode = d.UserRef,
                                    Approver = approver.EmpNo,
                                    Email = requestor.Wemail,
                                    ApproverEmail = approver.Wemail,
                                    ApproverTitle = approverResponse.Data?.Title,
                                    DocType = d.Type,
                                    Description = d.Description,
                                    Department = d.Department,
                                    Names = requestor.Names,
                                    ApproverLevel = approverResponse.Data.Level,
                                    ApproverUserCode = approverResponse.Data.UserCode,
                                    ApproverNames = approver.Names,
                                    TaskAction = approverResponse.Data.Action,
                                    TaskNotification = approverResponse.Data.TaskNotification
                                });
                            }
                    }

                });

                return Json(new ReturnData<List<NotificationRecipientVm>>
                {
                    Success = true,
                    Data = recipients
                });
            }
            catch (Exception ex)
            {
                return Json(new ReturnData<List<NotificationRecipientVm>>
                {
                    Success = false
                });
            }
        }

        private ReturnData<Approver> GetApprover(IEnumerable<Approver> approvedApprovers, WfdocCentre docCenter)
        {
            try
            {
                docCenter.LatestApprovalBy = docCenter?.LatestApprovalBy ?? "";
                if (approvedApprovers == null)
                    approvedApprovers = new List<Approver>();
                var lastApproverData = _context.Users.Select(u => new Users { 
                    EmpNo = u.EmpNo,
                    UserCode = u.UserCode,
                    Email = u.Email,
                    Names = u.Names
                }).FirstOrDefault(u => u.Names.ToUpper().Equals(docCenter.LatestApprovalBy.ToUpper()));
                if (lastApproverData == null)
                    lastApproverData = new Users();
                var lastApprover = approvedApprovers.OrderByDescending(a => a.Level).FirstOrDefault();
                if (lastApprover == null)
                    lastApprover = new Approver();
                var level = lastApprover.Level;
                lastApproverData.EmpNo = lastApproverData?.EmpNo ?? "";
                lastApprover.EmpNo = lastApprover?.EmpNo ?? "";
                var approvers = _context.WfdocCentreDetails.Where(d => d.Ref == $"{docCenter.Id}").ToList();
                var centerDetails = approvers.OrderBy(a => a.Level).FirstOrDefault();
                if (centerDetails == null)
                    centerDetails = new WfdocCentreDetails();
                centerDetails.Level = centerDetails?.Level ?? 0;
                centerDetails.ActionSelected = centerDetails?.ActionSelected ?? "";
                var docCentreTask = _context.WFDocCentreTasks.FirstOrDefault(c => c.Ref.ToUpper().Equals(docCenter.DocNo.ToUpper()));
                if (docCentreTask == null)
                    docCentreTask = new WFDocCentreTasks();
                docCentreTask.Task = docCentreTask?.Task ?? "";
                var currentApprover = approvers.FirstOrDefault(a => a.Level == level);
                if (currentApprover == null)
                    currentApprover = new WfdocCentreDetails();
                currentApprover.ActionSelected = currentApprover?.ActionSelected ?? "";
                var nextApprover = new Approver { };

                if (lastApproverData.EmpNo.ToUpper().Equals(lastApprover.EmpNo.ToUpper()) 
                    || (!string.IsNullOrEmpty(docCentreTask.Task) && !currentApprover.ActionSelected.ToLower().Equals("approved")))
                {
                    if (level < 1)
                        level =  (int)centerDetails.Level;

                    if (currentApprover.ActionSelected.ToLower().Equals("approved"))
                        ++level;

                    var arrApprovedApprovers = approvedApprovers.Select(a => a.UserCode.ToUpper()).ToList();
                    var unApprovedApprovers = approvers.Where(a => string.IsNullOrEmpty(a.ActionSelected) || !a.ActionSelected.ToLower().Equals("approved")).ToList();
                    var approver = unApprovedApprovers.FirstOrDefault(a => a.Level == level);
                    if (approver == null)
                        return new ReturnData<Approver>
                        {
                            Success = false,
                            Data = nextApprover
                        };

                    var user = _context.Users.Select(u => new Users
                    {
                        EmpNo = u.EmpNo,
                        UserCode = u.UserCode,
                        Email = u.Email,
                        Names = u.Names
                    }).FirstOrDefault(u => u.UserCode.ToUpper().Equals(approver.UserCode.ToUpper()));

                    if (user == null)
                        return new ReturnData<Approver>
                        {
                            Success = false,
                            Data = nextApprover
                        };

                    
                    nextApprover = new Approver
                    {
                        EmpNo = user.EmpNo,
                        Title = approver.Approver,
                        Level = (int)approver.Level,
                        UserCode = approver.UserCode,
                        Action = docCentreTask?.Task ?? "",
                        TaskNotification = lastApprover.TaskNotification
                    };
                }

                return new ReturnData<Approver>
                {
                    Success = true,
                    Data = nextApprover
                };
            }
            catch (Exception ex)
            {
                return new ReturnData<Approver>
                {
                    Success = false,
                    Data = new Approver { }
                };
            }

        }
    }
}