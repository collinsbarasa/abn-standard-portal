﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Unisol.Web.Api.IServices;
using Unisol.Web.Common.Process;
using Unisol.Web.Common.Utilities;
using Unisol.Web.Common.ViewModels.Sor;
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Api.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	[ApiController]
	public class IRController : Controller
	{
		private readonly UnisolAPIdbContext _context;
		private IStaffServices _staffServices;
		private Utils utils;

		public IRController(UnisolAPIdbContext context, IStaffServices staffServices)
		{
			_context = context;
			_staffServices = staffServices;
			utils = new Utils(context);
		}

		[HttpPost("[action]")]
		public JsonResult CreateIR(CreateIRModel createIR)
		{
			try
			{
				var RefNo = GenerateRefNo("IRQ");
				var employee = _context.HrpEmployee.FirstOrDefault(u => u.EmpNo ==  createIR.Details.Usercode);
				var user = _context.Users.FirstOrDefault(u => u.EmpNo == employee.EmpNo);
				if (user == null)
					user = new Users();

				var procOnlineReq = new ProcOnlineReq
				{
					ReqRef = RefNo,
					DocType = "Internal Requisition",
					Usercode = string.IsNullOrEmpty(user.UserCode) ? employee.EmpNo : user.UserCode,
					Reaction = "",
					Notes = createIR.Details.Notes,
					Rdate = DateTime.UtcNow.Date,
					Rtime = DateTime.UtcNow.AddHours(3),
					Status = "Pending"
				};

				var wfRouting = _staffServices.GetWfRouting(procOnlineReq.DocType);
				if (!wfRouting.Success)
				{
					procOnlineReq.DocType = "STORES REQUISITION";
					wfRouting = _staffServices.GetWfRouting(procOnlineReq.DocType);
					if(!wfRouting.Success)
						return Json(wfRouting);
				}

				if (string.IsNullOrEmpty(wfRouting.Data.Id.ToString()))
					return Json(wfRouting);

				var workFlowStatus = utils.SaveToWorkFlowCenter(procOnlineReq, employee, wfRouting.Data.Id.ToString());
				if (!workFlowStatus.Success)
					return Json(workFlowStatus);

				_context.ProcOnlineReq.Add(procOnlineReq);

				foreach (var irItem in createIR.Items)
				{
					_context.ProcOnlineReqDetail.Add(new ProcOnlineReqDetail
					{
						Amount = irItem.Totalamount,
						Cost = Convert.ToDecimal(irItem.Unitamount),
						Qty = Convert.ToDecimal(irItem.Quantity),
						ReqRef = procOnlineReq.ReqRef,
						UoM = irItem.Unitmeasure,
						Description = irItem.Description
					});
				}

				_context.SaveChanges();

				return Json(new ReturnData<ProcOnlineReq>
				{
					Success = true,
					Message = "Internal Requisition submited successfully"
				});
			}
			catch (Exception e)
			{
				return Json(new ReturnData<ProcOnlineReq>
				{
					Success = false,
					Message = "Sorry. Something went wrong.Please try again"
				});
			}
		}

		[HttpGet("[action]")]
		public JsonResult GetStaffIR(string usercode, string searchText)
		{
			var wfRouting = _staffServices.GetWfRouting("INTERNAL REQUISITION");
			if (!wfRouting.Success)
			{
				wfRouting = _staffServices.GetWfRouting("STORES REQUISITION");
				if(!wfRouting.Success)
					return Json(wfRouting);
			}

			var onlineRequestResponse = _staffServices.GetOnlineRequest("IRQ");
			if (!onlineRequestResponse.Success)
				return Json(onlineRequestResponse);

			var procItems = _staffServices.GetProcItems(searchText);
			if (!procItems.Success)
				return Json(procItems);

			var user = _context.Users.FirstOrDefault(u => u.EmpNo == usercode);
			if (user == null)
				user = new Users();
			user.UserCode = user?.UserCode ?? "";

			var InternalRequitions = onlineRequestResponse.Data.Where(r => r.Usercode == usercode || r.Usercode == user.UserCode)
				.OrderByDescending(r => r.Rtime).ToList();
			var requisitions = new List<IRDataVm>();
			InternalRequitions.ForEach(r => {
				var docCenterDetails = utils.GetDocCenterDetails(r.ReqRef);
				requisitions.Add(new IRDataVm
				{
					ReqRef = r.ReqRef,
					DocType = r.DocType,
					Usercode = r.Usercode,
					Rdate= r.Rdate,
					Rtime = r.Rtime,
					Reaction= r.Reaction,
					Reactby = r.Reactby,
					ReactDate = r.ReactDate,
					ReactTime = r.ReactTime,
					Notes = r.Notes,
					Status = r.Status,
					Reason = docCenterDetails?.Reason ?? ""
				});
			});

			return Json(new ReturnData<IRProcDataVm>
			{
				Success = true,
				Data = new IRProcDataVm
				{
					Requisitions = requisitions,
					ProcItems = procItems.Data
				}
			});
		}

		private string GenerateRefNo(string docType)
		{
			var onlineRequestResponse = _staffServices.GetOnlineRequest(docType);
			if (!onlineRequestResponse.Success || onlineRequestResponse.Data.Count < 1)
				return "IRQ001";
			
			var onlineRequest = onlineRequestResponse.Data
				.OrderByDescending(i => Convert.ToInt32(i.ReqRef.Substring(3))).FirstOrDefault();

			var count = onlineRequest.ReqRef.Count();
			var digits = onlineRequest.ReqRef.Substring(3, count - 3);
			var sufix = Convert.ToInt32(digits);

			sufix++;

			var RefNo = "IRQ";
			if (sufix < 10) RefNo += "00" + sufix;

			if ((sufix > 9) && (sufix < 100)) RefNo += "0" + sufix;

			if (sufix > 99) RefNo += "" + sufix;
			return RefNo;
		}

		[HttpPost("[action]")]
		public JsonResult AddItems(AddItems addItems)
		{
			try
			{
				foreach (var item in addItems.Soritems)
				{
					if (item.Id == 0)
					{
						var proqDetails = new ProcOnlineReqDetail();
						proqDetails.Amount = item.Totalamount;
						proqDetails.Cost = Convert.ToDecimal(item.Unitamount);
						proqDetails.Qty = Convert.ToDecimal(item.Quantity);
						proqDetails.ReqRef = addItems.RefReq;
						proqDetails.UoM = item.Unitmeasure;
						proqDetails.Description = item.Description;
						_context.ProcOnlineReqDetail.Add(proqDetails);
						_context.SaveChanges();
					}

					if (item.Id != 0)
					{
						var preqDetail = _context.ProcOnlineReqDetail.FirstOrDefault(x => x.Id == item.Id);
						preqDetail.Amount = item.Totalamount;
						preqDetail.Cost = Convert.ToDecimal(item.Unitamount);
						preqDetail.Qty = Convert.ToDecimal(item.Quantity);
						preqDetail.ReqRef = addItems.RefReq;
						preqDetail.UoM = item.Unitmeasure;
						preqDetail.Description = item.Description;
						_context.SaveChanges();
					}
				}
				return Json(new ReturnData<string>
				{
					Success = true,
					Message = "Items added successfully"
				});
			}
			catch (Exception ex)
			{
				return Json(new ReturnData<string>
				{
					Success = false,
					Message = "Sorry something went wrong while adding IR items, please try again",

				});
			}
		}
	}
}
