﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Unisol.Web.Api.IRepository;
using Unisol.Web.Common.Utilities;
using Unisol.Web.Common.ViewModels.Academics;
using Unisol.Web.Entities.Database.UnisolModels;

namespace Unisol.Web.Api.Repository
{
	public class MarkSheetRepository : GenericRepository<MarkSheet>, IMarkSheetRepository
	{
		private UnisolAPIdbContext _context;
		public MarkSheetRepository(UnisolAPIdbContext context) : base(context)
		{
			_context = context;
		}

		public IEnumerable<MarksheetResults> GetStudentResults(TranscriptRequestViewModel transcriptModel, string institutionInitials,
			IConfiguration configuration)
		{
			var sysSetup = _context.SysSetup.FirstOrDefault();
			var results = _context.MarkSheet.Join(_context.Subjects,
					m => m.Subject,
					s => s.Code,
					(m, s) => new MarksheetResults
					{
						AdmnNo = m.AdmnNo,
						Term = m.Term,
						Code = m.Subject,
						Title = s.Names,
						Exam = m.Exam,
						
					}).Where(r => r.AdmnNo == transcriptModel.Usercode).ToList();

			if (institutionInitials.ToUpper().Equals("KCNP") || institutionInitials.ToUpper().Equals("NTTI"))
			{
				string connetionString = DbSetting.ConnectionString(configuration, "Unisol");
				SqlConnection connection = new SqlConnection(connetionString);
				if (connection.State == ConnectionState.Closed)
					connection.Open();

				var marks = "SELECT m.admnno, m.term, m.subject, s.names, m.exam, m.[cat1], m.[cat2] FROM MarkSheet m INNER JOIN Subjects s ON m.subject = s.code " +
					"WHERE m.admnno = '" + transcriptModel.Usercode + "'";
				if (institutionInitials.ToUpper().Equals("NTTI"))
					marks = "SELECT m.admnno, m.term, m.subject, s.names, m.exam, m.[cat 1], m.[cat 2] FROM MarkSheet m INNER JOIN Subjects s ON m.subject = s.code " +
						"WHERE m.admnno = '" + transcriptModel.Usercode + "'";
				var sqlCommand = new SqlCommand(marks, connection);
				var reader = sqlCommand.ExecuteReader();
				results = new List<MarksheetResults>();
				while (reader.Read())
				{
					decimal cat1 = 0, cat2 = 0, exam = 0;
					decimal.TryParse(reader[5].ToString(), out cat1);
					decimal.TryParse(reader[6].ToString(), out cat2);
					decimal.TryParse(reader[4].ToString(), out exam);
					var cat = (cat1 + cat2) * 0.5m;
					if (institutionInitials.ToUpper().Equals("NTTI"))
					{
						cat = (sysSetup.OtherTests / 100) * cat1 ?? 0;
						exam = (sysSetup.Exam / 100) * exam ?? 0;
					}

					results.Add(new MarksheetResults
					{
						AdmnNo = reader[0].ToString(),
						Term = reader[1].ToString(),
						Code = reader[2].ToString(),
						Title = reader[3].ToString(),
						Exam = Math.Round(exam, MidpointRounding.AwayFromZero),
						Cat = Math.Round(cat, MidpointRounding.AwayFromZero)
					});
				}

				sqlCommand.Dispose();
				connection.Close();
			}

			return results;
		}

		public IEnumerable<MarksheetResults> GetStudentResults(TranscriptRequestViewModel transcriptModel)
		{
			var sysSetup = _context.SysSetup.FirstOrDefault();
			var results = new List<MarksheetResults>();

			var markTypes = _context.MarkType.Where(x => x.Closed != true && !(x.Names.Contains("exam"))).ToList();
			var list = _context.MarkSheet.Join(_context.Subjects,
					m => m.Subject,
					s => s.Code,
					(m, s) => new MarksheetResults
					{
						ID = m.Id,
						AdmnNo = m.AdmnNo,
						Term = m.Term,
						Code = m.Subject,
						Title = s.Names,
						Exam = m.Exam,
					}).Where(r => r.AdmnNo == transcriptModel.Usercode && transcriptModel.Terms.Contains(r.Term.ToUpper())).ToList();
					
					list.ForEach(item => {
						decimal otherTestMark = 0.0m;
						var applicableTypes = GetApplicableOtherTests(markTypes, item);
						applicableTypes.ForEach(otherTest => {
							var query = "SELECT ["+otherTest.Names+ "] from MarkSheet WHERE ID="+item.ID;
							decimal tempTotal = 0.0m;
								using (var command = _context.Database.GetDbConnection().CreateCommand())
								{
									command.CommandText = query;
									command.CommandType = CommandType.Text;

									_context.Database.OpenConnection();

									using (var result = command.ExecuteReader())
									{
										
										while (result.Read())
										{
											if (result[0] != null) {
												decimal temp = 0.0m;
												decimal.TryParse(result[0].ToString(), out temp);
												tempTotal = tempTotal + temp;
											}
										}
									}
								}
							
							otherTestMark = otherTestMark + tempTotal;
						});

						if (sysSetup.OtherTestsTreat == "Sum and Average") {
							otherTestMark = otherTestMark / applicableTypes.Count;
							//otherTestMark = Math.Round((otherTestMark / 100) * (decimal)sysSetup.OtherTests);
							//item.Exam = Math.Round((decimal)item.Exam / 100 * (decimal)sysSetup.Exam);
						}
						
						item.Cat = otherTestMark;
						if (item.Exam == null) {
							item.Exam = 0.0m;
							item.ExamIsNull = true;
						}
						if (item.Cat == null)
						{
							item.Cat = 0.0m;
							item.CatIsNull = true;
						}
						
						//item.Cat = otherTestMark;
						results.Add(item);
					});
			return results;
		}

		public List<MarkType> GetApplicableOtherTests(List<MarkType> markTypes, MarksheetResults item) {
			var applicableTestTypes = new List<MarkType>();
			markTypes.ForEach(otherTest => {
				var query = "SELECT [" + otherTest.Names + "] from ClassCourse WHERE Term='" + item.Term +"' AND Subject='"+item.Code+"'";
				
				using (var command = _context.Database.GetDbConnection().CreateCommand())
				{
					command.CommandText = query;
					command.CommandType = CommandType.Text;

					_context.Database.OpenConnection();

					using (var result = command.ExecuteReader())
					{
						bool applicable = true;
						while (result.Read())
						{
							if (result[0] != null)
							{
								if (result[0].ToString().ToUpper() == "FALSE") {
									applicable = false;
								}
								
							}
						}
						if (applicable) {
							applicableTestTypes.Add(otherTest);
						}
					}
				}

			});
			return applicableTestTypes;
		}
			
}
}